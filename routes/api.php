<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\DepartmentController;
use App\Http\Controllers\AppController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->group(function (){
    Route::get('/departments',[DepartmentController::class,"index"]);
    Route::get('/departments/{department}',[DepartmentController::class,"show"]);
    Route::put('/departments/{department}',[DepartmentController::class,"update"]);
    Route::delete('/departments/{department}',[DepartmentController::class,"destroy"]);
    Route::post('/departments',[DepartmentController::class,"store"]);
});

