<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Employee;
use Illuminate\Http\Request;

class AppController extends Controller
{
    public function index(){
        return view("index");
    }

}
