<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DepartmentResource;
use App\Models\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    public function index(){
        return DepartmentResource::collection(Department::paginate(10));
    }

    public function show(Department $department){
        return new DepartmentResource($department);
    }

    public function update(Department $department,Request $request){
        $data=$request->validate([
           'name'=>'required',
        ]);

        $department->update($data);
        return new DepartmentResource($department);
    }

    public function destroy(Department $department){
        $department->delete();

        return response(null,204);
    }

    public function store(Request $request){
        $data=$request->validate([
           'name'=>"required",
        ]);

        return new DepartmentResource(Department::create([
            'name'=>$data['name'],
        ]));

    }

}

