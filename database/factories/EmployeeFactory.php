<?php

namespace Database\Factories;

use App\Models\Employee;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Employee::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
            "department_id"=>1,
            "position_id"=>2,
            "firstName"=>$this->faker->firstName,
            "lastName"=>$this->faker->lastName,
            "identificationNumber"=>$this->faker->numerify("#######"),
            "startDate"=>$this->faker->date()
        ];
    }
}
