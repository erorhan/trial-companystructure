<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->increments("id");
            $table->integer("department_id")->unsigned();
            $table->integer("position_id")->unsigned();
            $table->string("firstName");
            $table->string("lastName");
            $table->string("identificationNumber")->unique();
            $table->date("startDate");
            $table->date("endDate")->nullable();
            $table->float("salary");
            $table->timestamps();
            $table->softDeletes();

            $table->foreign("department_id")->references("id")->on("department")->onDelete("cascade");
            $table->foreign("position_id")->references("id")->on("position")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
