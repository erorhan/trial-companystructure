
import axios from "axios";

export default {
    all(params){
        return axios.get("/api/departments",{params});
    },
    find(id){
        return axios.get("/api/departments/"+ id);
    },
    update(id,data){
        return axios.put("/api/departments/"+ id,data);
    },
    delete(id){
        return axios.delete("/api/departments/"+id);
    },
    create(data){
        return axios.post('/api/departments/',data);
    }
};
