

require('./bootstrap');

import Vue from 'vue'
import VueRouter from "vue-router";

Vue.use(VueRouter);

import  {routes} from "./routes";
const router=new VueRouter({
    mode:'history',
    routes
});

//App and Home
import App from './components/App';


const app = new Vue({
    el: '#app',
    components:{App},
    router
});
