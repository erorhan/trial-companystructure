// Department
import Dep_AddComponent from './components/department/AddComponent';
import Dep_ListComponent from './components/department/ListComponent';
import Dep_UpdateComponent from './components/department/UpdateComponent';
import Dep_ViewComponent from './components/department/ViewComponent';
// Employee
import Emp_AddComponent from './components/employee/AddComponent';
import Emp_ListComponent from './components/employee/ListComponent';
import Emp_UpdateComponent from './components/employee/UpdateComponent';
import Emp_ViewComponent from './components/employee/ViewComponent';
// Position
import Pos_AddComponent from './components/position/AddComponent';
import Pos_ListComponent from './components/position/ListComponent';
import Pos_UpdateComponent from './components/position/UpdateComponent';
import Pos_ViewComponent from './components/position/ViewComponent';

// Home
import Home from './components/Home';
import NotFound from './components/NotFound';

export const routes=[
        {
            path: '/',
            name: 'home',
            component: Home
        },
        // NotFound
        {
            path:'/404',
            name:'404',
            component:NotFound,
        },
        {
            path:'*',
            redirect:'/404',
        },
        // Department
        {
            path: '/department-list',
            name: 'department-list',
            component: Dep_ListComponent,
        },
        {
            path: '/department-add',
            name: 'department-add',
            component: Dep_AddComponent,
        },
        {
            path: '/department-update/:id',
            name: 'department-update',
            component: Dep_UpdateComponent,
        },
        {
            path: '/department-view',
            name: 'department-view',
            component: Dep_ViewComponent,
        },
        //Employee
        {
            path: '/employee-list',
            name: 'employee-list',
            component: Emp_ListComponent,
        },
        {
            path: '/employee-add',
            name: 'employee-add',
            component: Emp_AddComponent,
        },
        {
            path: '/employee-update',
            name: 'employee-update',
            component: Emp_UpdateComponent,
        },
        {
            path: '/employee-view',
            name: 'employee-view',
            component: Emp_ViewComponent,
        },
        //Position
        {
            path: '/position-list',
            name: 'position-list',
            component: Pos_ListComponent,
        },
        {
            path: '/position-add',
            name: 'position-add',
            component: Pos_AddComponent,
        },
        {
            path: '/position-update',
            name: 'position-update',
            component: Pos_UpdateComponent,
        },
        {
            path: '/position-view',
            name: 'position-view',
            component: Pos_ViewComponent,
        }
];
